import Foundation

@Observable
class Recipe: Identifiable {
  let id: UUID = UUID()
  var name: String
  var details: String?
  var credit: String?
  var thumbnailUrl: URL?
  var cuisine: Cuisine
  var mealCourse: MealCourse
  var ingredients: [RecipeIngredient]
  var instructions: [Instruction]
  var sectionLabels: [String] = []
  var lastPreparedAt: Date?
  var notes: String

  init(name: String, details: String? = nil, credit: String? = nil, thumbnailUrl: URL? = nil, cuisine: Cuisine, mealCourse: MealCourse, ingredients: [RecipeIngredient], instructions: [Instruction], sectionLabels: [String] = [], lastPreparedAt: Date? = nil, notes: String = "") {
    self.name = name
    self.details = details
    self.credit = credit
    self.thumbnailUrl = thumbnailUrl
    self.cuisine = cuisine
    self.mealCourse = mealCourse
    self.ingredients = ingredients
    self.instructions = instructions
    self.sectionLabels = sectionLabels
    self.lastPreparedAt = lastPreparedAt
    self.notes = notes
  }

  var unsectionedIngredients: [RecipeIngredient] {
    ingredients
      .filter({ $0.sectionLabel == nil })
  }

  enum MealCourse: String {
    case appetizer
    case main
    case side
    case dessert
  }

  enum Cuisine {
    case american
    case chinese(region: String)
    case japanese
    case italian(winePairing: ItalianWine)
  }

  enum ItalianWine {
    case barolo
    case brunello
    case taurasi
  }

  static func mealCourseFilter(course: MealCourse, recipes: [Recipe]) -> [Recipe] {
    recipes.filter { $0.mealCourse == course }
  }

  func ingredientsForSectionLabel(_ sectionLabel: String) -> [RecipeIngredient] {
    ingredients
      .filter { $0.sectionLabel == sectionLabel }
      .sorted { $0.position < $1.position }
  }
}

struct RecipeIngredient: Identifiable {
  let id: UUID = UUID()
  var ingredient: Ingredient
  var quantity: Double?
  var unit: String?
  var note: String?
  var position: Int = 999999
  var sectionLabel: String?


}

struct Ingredient: Identifiable {
  let id: UUID = UUID()
  var name: String
}

struct Instruction: Identifiable {
  let id: UUID = UUID()
  var position: Int = 999999
  var instructionText: String
}
