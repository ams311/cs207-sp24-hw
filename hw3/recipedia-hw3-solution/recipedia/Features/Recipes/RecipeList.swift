import SwiftUI

struct RecipeList: View {
  let recipes: [Recipe] = Recipe.previewData

  var body: some View {
    NavigationView {
      List(recipes) { recipe in
        NavigationLink(destination: RecipeDetail(recipe: recipe)) {
          RecipeRow(recipe: recipe)
        }
      }
      .navigationTitle("Recipes - jrp96")
    }
  }
}

struct RecipeRow: View {
  let recipe: Recipe

  var body: some View {
    HStack(alignment: .top) {
      thumbnail
      VStack(alignment: .leading) {
        Text(recipe.mealCourse.rawValue.uppercased()).font(.caption)
        Text(recipe.name)
          .font(.headline)
          .padding(.bottom, 3)
      }
      Spacer()
      preparedCheckmark
    }
  }

  var thumbnail: some View {
    AsyncImage(url: recipe.thumbnailUrl) { image in
      image
        .resizable()
        .cornerRadius(6)
    } placeholder: {
      if recipe.thumbnailUrl != nil {
        ProgressView()
      } else {
        Image(systemName: "fork.knife")
      }
    }
    .frame(width: 50, height: 50)
  }

  var preparedCheckmark: some View {
    VStack {
      Spacer()
      Image(systemName: recipe.lastPreparedAt != nil ? "checkmark.circle.fill" : "circle")
        .foregroundColor(recipe.lastPreparedAt != nil ? .green : .black)
      Spacer()
    }
  }
}

#Preview {
    RecipeList()
//    RecipeList()
//      .preferredColorScheme(.dark)
//    RecipeList()
//      .environment(\.sizeCategory, .extraLarge)
}
