//
//  hw3App.swift
//  hw3
//
//  Created by ams311 on 2/13/24.
//

import SwiftUI

@main
struct RecipeAPP: App {
    var body: some Scene{
        WindowGroup{
            RecipeList(recipes: Binding.constant(Recipe.previewData))
        }
    }
}
