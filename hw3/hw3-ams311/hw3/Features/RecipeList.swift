import SwiftUI
struct RecipeList: View {
    @Binding var recipes: [Recipe]

    var body: some View {
        NavigationStack {
            List(recipes) { recipe in
                NavigationLink(destination: RecipeDetail(recipe: recipe)) {
                    HStack {
                        AsyncImage(url: recipe.thumbnailUrl) { image in
                            image.resizable().aspectRatio(contentMode: .fit)
                        } placeholder: {
                            ProgressView()
                        }
                        .frame(width: 50, height: 50)
                        VStack(alignment: .leading) {
                            Text(recipe.name).font(.headline)
                            Text(recipe.mealCourse.rawValue) // Assuming mealCourse is an enum
                        }
                        Spacer()
                        Image(systemName: recipe.lastPreparedAt != nil ? "checkmark.circle.fill" : "circle")
                            .foregroundColor(recipe.lastPreparedAt != nil ? .green : .gray)
                        
                    }
                }
                .swipeActions {
                    Button(role: .destructive) {
                        deleteRecipe(recipe)
                    } label: {
                        Label("Delete", systemImage: "trash")
                    }
                }
            }
            .navigationTitle("Recipes-ams311")
        }
    }
    private func deleteRecipe(_ recipe: Recipe) {
        guard let index = recipes.firstIndex(where: { $0.id == recipe.id }) else { return }
        recipes.remove(at: index)
    }
}
#Preview {
    RecipeList(recipes: Binding.constant(Recipe.previewData))
}


