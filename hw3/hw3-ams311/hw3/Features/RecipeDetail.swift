import SwiftUI
struct RecipeDetail: View {
    @Bindable var recipe: Recipe
    @State private var scale: Double = 1.0
    @State private var editingNotes: Bool = false

    private func scaledQuantity(for ingredient: RecipeIngredient) -> Double {
        (ingredient.quantity ?? 0) * scale
    }
    
    private func ingredientText(_ ingredient: RecipeIngredient) -> String {
        let scaledQty = scaledQuantity(for: ingredient)
        if scaledQty == 0 {
            // When quantity is 0, check if the unit contains descriptive text (like "handful of").
            // If so, include it along with the ingredient name.
            // This assumes that any descriptive unit doesn't require a numeric quantity.
            if let unit = ingredient.unit, !unit.isEmpty {
                return "\(unit) \(ingredient.ingredient.name)"
            } else {
                // If there's no unit or it's not descriptive, return just the ingredient name.
                return ingredient.ingredient.name
            }
        } else {
            // Use formatted quantity when it's not 0 and ensure unit is included if it exists.
            let formattedQuantity = String(format: "%.2f", scaledQty)
            let unitPart = ingredient.unit.map { "\($0) " } ?? ""
            return "\(formattedQuantity) \(unitPart)\(ingredient.ingredient.name)"
        }
    }

    var body: some View {
        ScrollView {
            AsyncImage(url: recipe.thumbnailUrl) { image in
                image.resizable().aspectRatio(contentMode: .fit)
            } placeholder: {
                ProgressView()
            }
            .frame(height: 300)

            Text(recipe.cuisineDisplay())
                .font(.headline)
                .padding(.bottom, 20)

            Text(recipe.name)
                .font(.largeTitle)
            HStack {
                            Button(action: {
                                recipe.lastPreparedAt = recipe.lastPreparedAt == nil ? Date() : nil
                            }) {
                                HStack {
                                    Text(recipe.lastPreparedAt == nil ? "Mark as Prepared" : "Unmark Preparation")
                                    
                                    // Conditionally display a checkmark when the recipe has been prepared
                                    if recipe.lastPreparedAt != nil {
                                        Image(systemName: "checkmark")
                                            .foregroundColor(.green)
                                    }
                                }
                            }
                        }

            Picker("Scale the recipe", selection: $scale) {
                Text("0.5x").tag(0.5)
                Text("1x").tag(1.0)
                Text("2x").tag(2.0)
            }
            .pickerStyle(.segmented)
            .padding()

            if recipe.sectionLabels.isEmpty {
                ForEach(recipe.ingredients) { ingredient in
                    Text(ingredientText(ingredient))
                }
            } else {
                ForEach(recipe.sectionLabels, id: \.self) { sectionLabel in
                    VStack(alignment: .leading) {
                        Text(sectionLabel)
                            .font(.headline)
                            .padding(.top, 20)
                        
                        ForEach(recipe.ingredients.filter { $0.sectionLabel == sectionLabel }) { ingredient in
                            Text(ingredientText(ingredient))
                        }
                    }
                }
            }

            VStack(alignment: .leading) {
                Text("Instructions")
                    .font(.headline)
                    .padding(.vertical, 5)
                    .padding(.horizontal, 5)
                
                ForEach(recipe.instructions) { instruction in
                    Text(instruction.instructionText)
                        .padding(.bottom, 5)
                        .padding(.horizontal, 5)
                }

                Text("Notes")
                    .font(.headline)
                    .padding(.top, 20)

                if editingNotes {
                    TextEditor(text: $recipe.notes)
                        .frame(height: 200)
                        .padding()
                        .border(Color.gray, width: 1)
                } else {
                    Text(recipe.notes)
                        .padding()
                        .onTapGesture {
                            editingNotes = true
                        }
                }
            }
        }
        .navigationTitle("Recipe Details")
        .toolbar {
            ToolbarItem(placement: .navigationBarTrailing) {
                Button(editingNotes ? "Done" : "Edit Notes") {
                    editingNotes.toggle()
                }
            }
        }
    }
}

extension Recipe {
    func cuisineDisplay() -> String {
        switch self.cuisine {
        case .italian(let winePairing):
            return "Italian, Wine Pairing: \(winePairing.rawValue)"
        case .chinese(let region):
            return "Chinese, Region: \(region)"
        case .american:
            return "American"
        case .japanese:
            return "Japanese"
        }
    }
}
#Preview {
  RecipeDetail(recipe: Recipe.previewData[0])
}
