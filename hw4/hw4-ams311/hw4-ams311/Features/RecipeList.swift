import SwiftUI
import SwiftData

struct RecipeList: View {
    @Environment(\.modelContext) private var modelContext
    var recipes: [Recipe]
    @State private var isPresentingRecipeForm: Bool = false
    @State private var newRecipeFormData = Recipe.FormData()
    
    var body: some View {
        List(recipes) { recipe in
            NavigationLink(destination: RecipeDetail(recipe: recipe)) {
                HStack {
                    AsyncImage(url: recipe.thumbnailUrl) { image in
                        image.resizable().aspectRatio(contentMode: .fit)
                    } placeholder: {
                        ProgressView()
                    }
                    .frame(width: 50, height: 50)
                    VStack(alignment: .leading) {
                        Text(recipe.name).font(.headline)
                        Text(recipe.mealCourse.rawValue)
                    }
                    Spacer()
                    Image(systemName: recipe.lastPreparedAt != nil ? "checkmark.circle.fill" : "circle")
                        .foregroundColor(recipe.lastPreparedAt != nil ? .green : .gray)
                }
            }
        }
        .navigationTitle("Recipes")
        .toolbar {
            ToolbarItem(placement: .navigationBarTrailing) {
                Button("Add") {
                    isPresentingRecipeForm.toggle()
                }
            }
        }
        .sheet(isPresented: $isPresentingRecipeForm) {
            NavigationStack {
                RecipeForm(data: $newRecipeFormData)
                    .toolbar {
                        ToolbarItem(placement: .navigationBarLeading) {
                            Button("Cancel") {
                                isPresentingRecipeForm.toggle()
                                newRecipeFormData = Recipe.FormData()
                            }
                        }
                        ToolbarItem(placement: .navigationBarTrailing) {
                            Button("Save") {
                                Recipe.create(from: newRecipeFormData, context: modelContext)
                                newRecipeFormData = Recipe.FormData()
                                isPresentingRecipeForm.toggle()
                            }
                        }
                    }
                    .navigationTitle("Add Recipe")
            }
        }
    }
    
}
#Preview {
  let preview = PreviewContainer([Recipe.self])
  preview.add(items: Recipe.previewData)
  return NavigationView {
    RecipeList(recipes: Recipe.previewData)
      .modelContainer (preview.container)
  }}
