import SwiftUI
struct RecipeDetail: View {
    @Bindable var recipe: Recipe
    @State private var scale: Double = 1.0
    @State private var editRecipeFormData: Recipe.FormData = Recipe.FormData()
    @State private var isPresentingRecipeForm: Bool = false

    
    private func scaledQuantity(for ingredient: RecipeIngredient) -> Double {
        (ingredient.quantity ?? 0) * scale
    }
    
    private func ingredientText(_ ingredient: RecipeIngredient) -> String {
        let scaledQty = scaledQuantity(for: ingredient)
        if scaledQty == 0 {
            if let unit = ingredient.unit, !unit.isEmpty {
                return "\(unit) \(ingredient.ingredient.name)"
            } else {
                // If there's no unit or it's not descriptive, return just the ingredient name.
                return ingredient.ingredient.name
            }
        } else {
            // Use formatted quantity when it's not 0 and ensure unit is included if it exists.
            let formattedQuantity = String(format: "%.2f", scaledQty)
            let unitPart = ingredient.unit.map { "\($0) " } ?? ""
            return "\(formattedQuantity) \(unitPart)\(ingredient.ingredient.name)"
        }
    }
    
    var body: some View {
        ScrollView {
            AsyncImage(url: recipe.thumbnailUrl) { image in
                image.resizable().aspectRatio(contentMode: .fit)
            } placeholder: {
                ProgressView()
            }
            .frame(height: 300)
            
            Text(recipe.name)
                .font(.largeTitle)
            HStack {
                Button(action: {
                    recipe.prepared.toggle()
                }) {
                    HStack {
                        Text(recipe.prepared ? "Mark as Unprepared" : "Mark as Prepared")
                        
                        // Display a checkmark if the recipe is marked as prepared
                        if recipe.prepared {
                            Image(systemName: "checkmark")
                                .foregroundColor(.green)
                        }
                    }
                }
            }
            
            Picker("Scale the recipe", selection: $scale) {
                Text("0.5x").tag(0.5)
                Text("1x").tag(1.0)
                Text("2x").tag(2.0)
            }
            .pickerStyle(.segmented)
            .padding()
            
            if recipe.sectionLabels.isEmpty {
                ForEach(recipe.ingredients) { ingredient in
                    Text(ingredientText(ingredient))
                }
            } else {
                ForEach(recipe.sectionLabels, id: \.self) { sectionLabel in
                    VStack(alignment: .leading) {
                        Text(sectionLabel)
                            .font(.headline)
                            .padding(.top, 20)
                        
                        ForEach(recipe.ingredients.filter { $0.sectionLabel == sectionLabel }) { ingredient in
                            Text(ingredientText(ingredient))
                        }
                    }
                }
            }
            
            VStack(alignment: .leading) {
                Text("Instructions")
                    .font(.headline)
                    .padding(.vertical, 5)
                    .padding(.horizontal, 5)
                
                ForEach(recipe.instructions) { instruction in
                    Text(instruction.instructionText)
                        .padding(.bottom, 5)
                        .padding(.horizontal, 5)
                }
                
                Text("Notes")
                       .font(.headline)
                       .padding(.top, 20)
                   
                TextFieldWithLabel(label: "", text: $recipe.notes, prompt: "Notes")

                
            }
        }
        .navigationTitle("Recipe Details")
        .toolbar {
          ToolbarItem(placement: .navigationBarTrailing) {
            Button("Edit") {
              editRecipeFormData = recipe.dataForForm
              isPresentingRecipeForm.toggle()
            }
          }
        }
        .sheet(isPresented: $isPresentingRecipeForm) {
          NavigationStack {
            RecipeForm(data: $editRecipeFormData)
              .toolbar {
                ToolbarItem(placement: .navigationBarLeading) {
                  Button("Cancel") { isPresentingRecipeForm.toggle() }

                }
                ToolbarItem(placement: .navigationBarTrailing) {
                  Button("Save") {
                    Recipe.update(recipe, from: editRecipeFormData)
                    isPresentingRecipeForm.toggle()
                  }
                }
              }
          }
        }
    }
}

#Preview {
  let preview = PreviewContainer([Recipe.self])
  let recipe = Recipe.previewData[0]
  return NavigationStack {
    RecipeDetail(recipe: recipe)
      .modelContainer (preview.container)
  }
}
