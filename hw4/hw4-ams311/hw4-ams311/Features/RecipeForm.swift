import SwiftUI

struct RecipeForm: View {
    @Binding var data: Recipe.FormData

    var body: some View {
        Form {
            TextFieldWithLabel(label: "Name", text: $data.name, prompt: "Enter a Title")

            Picker(selection: $data.mealCourse, label: Text("Mealcourse")) {
              ForEach(Recipe.MealCourse.allCases) { mealcourse in
                Text(mealcourse.rawValue)
              }
            }
            .pickerStyle(.menu)
            TextFieldWithLabel(label: "Details", text: $data.details, prompt: "Details")
            TextFieldWithLabel(label: "Credit", text: $data.credit, prompt: "Credits")
            
            Toggle("Previously Prepared", isOn: $data.prepared)
            
            if data.prepared {
                DatePicker(
                    "Last Prepared",
                    selection: Binding<Date>(
                        get: { self.data.lastPreparedAt ?? Date() }, // Provide a current date if nil
                        set: { self.data.lastPreparedAt = $0 }
                    ),
                    displayedComponents: .date
                )
            }
            VStack(alignment: .leading) {
              Text("Thumbnail URL")
                .bold()
                .font(.caption)
              TextField("Thumbnail URL", text: $data.thumbnailUrl, prompt: Text("Enter a URL"))
            }
            TextFieldWithLabel(label: "Notes", text: $data.notes, prompt: "Notes")
            
        }
    }
}

#Preview {
  let preview = PreviewContainer([Recipe.self])
  let data = Binding.constant(Recipe.previewData[0].dataForForm)
  return RecipeForm(data: data)
      .modelContainer(preview.container)
}
