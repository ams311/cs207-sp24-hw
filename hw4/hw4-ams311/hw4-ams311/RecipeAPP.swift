import SwiftUI
import SwiftData

@main
struct RecipeApp: App {

    let previewContainer = PreviewContainer([Recipe.self])
    
    init() {
        // Populate the preview container with preview data.
        // This is typically only for preview purposes, so you might
        // want to load data differently for your actual app.
        previewContainer.add(items: Recipe.previewData as [any PersistentModel])
    }
    
    var body: some Scene {
        WindowGroup {
            // Pass the preview data directly for demonstration.
            // In a real app, you'd fetch data from your modelContext.
            NavigationView {
                RecipeList(recipes: Recipe.previewData)
            }
        }
    }
}

