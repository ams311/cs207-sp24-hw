
import Foundation
import Combine

// MARK: - NewsResponse
struct NewsResponse: Decodable {
    var status: String
    var totalResults: Int
    var articles: [Article]
}

// MARK: - Article
struct Article: Decodable, Identifiable, Hashable {
    var source: Source
    var author: String?
    var title: String?
    var description: String?
    var url: String
    var urlToImage: String?
    var publishedAt: String
    var content: String?
    
    // Using URL as a unique identifier for each article
    var id: String { url }
}

// MARK: - Source
struct Source: Decodable, Hashable {
    var id: String?
    var name: String
}

// MARK: - NewsViewModel
class NewsViewModel: ObservableObject {
    @Published var articles = [Article]()
    @Published var isLoading = false
    @Published var errorMessage: String?
    
    private var apiClient: NewsAPI // Use the protocol type here
    
    // Modify the initializer to accept a NewsAPI
    init(apiClient: NewsAPI) {
        self.apiClient = apiClient
    }
    
    func loadTopHeadlines(country: String) {
        isLoading = true
        apiClient.fetchTopHeadlines(country: country) { [weak self] result in
            DispatchQueue.main.async {
                self?.isLoading = false
                switch result {
                case .success(let newsResponse):
                    self?.articles = newsResponse.articles
                case .failure(let error):
                    self?.errorMessage = error.localizedDescription
                }
            }
        }
    }
    
    func searchArticles(query: String) {
        isLoading = true
        errorMessage = nil
        
        apiClient.searchForArticles(with: query) { [weak self] result in
            DispatchQueue.main.async {
                self?.isLoading = false
                switch result {
                case .success(let newsResponse):
                    self?.articles = newsResponse.articles
                case .failure(let error):
                    self?.errorMessage = error.localizedDescription
                }
            }
        }
    }
}
