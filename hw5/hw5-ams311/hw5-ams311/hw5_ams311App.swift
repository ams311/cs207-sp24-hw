//
//  hw5_ams311App.swift
//  hw5-ams311
//
//  Created by ams311 on 2/27/24.
//

import SwiftUI

@main
struct hw5_ams311App: App {
    // Initialize your API client here
    let apiClient = NewsAPIClient() // Ensure RealNewsAPIClient conforms to NewsAPI
    
    var body: some Scene {
        WindowGroup {
            // Pass the apiClient to the NewsViewModel
            MainView(viewModel: NewsViewModel(apiClient: apiClient))
        }
    }
}
