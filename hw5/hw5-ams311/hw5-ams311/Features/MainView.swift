import SwiftUI
struct MainView: View {
    @StateObject var viewModel: NewsViewModel

    @State private var isShowingSearchView = false

    var body: some View {
        NavigationView {
            List {
                if viewModel.isLoading {
                    ProgressView("Loading...")
                } else if let errorMessage = viewModel.errorMessage {
                    Text(errorMessage)
                        .foregroundColor(.red)
                } else {
                    ForEach(viewModel.articles, id: \.self) { article in
                        NavigationLink(destination: ArticleDetailView(article: article)) {
                            ArticleRow(article: article)
                        }
                    }
                }
            }
            .navigationTitle("News")
            .toolbar {
                ToolbarItem(placement: .navigationBarTrailing) {
                    Button(action: {
                        isShowingSearchView = true
                    }) {
                        Image(systemName: "magnifyingglass")
                    }
                }
            }
            .sheet(isPresented: $isShowingSearchView) {
                // Make sure to pass the viewModel correctly
                SearchView(viewModel: viewModel, isPresented: $isShowingSearchView)
            }
        }
        .onAppear {
            viewModel.loadTopHeadlines(country: "us")
        }
    }
}

struct ArticleRow: View {
    let article: Article

    var body: some View {
        HStack(alignment: .top, spacing: 10) {
            AsyncImage(url: URL(string: article.urlToImage ?? "")) { image in
                image.resizable()
            } placeholder: {
                Image(systemName: "photo")
                    .resizable()
                    .scaledToFit()
                    .frame(width: 100, height: 60)
                    .foregroundColor(.secondary)
            }
            .frame(width: 100, height: 100)

            VStack(alignment: .leading) {
                Text(article.title ?? "Untitled")
                    .font(.headline)
                Text(article.description ?? "No description available.")
                    .font(.subheadline)
                    .foregroundColor(.secondary)
            }
        }
        .padding(.vertical, 4)
    }
}

struct ArticleDetailView: View {
    let article: Article

    var body: some View {
        ScrollView {
            VStack(alignment: .leading) {
                if let imageURL = URL(string: article.urlToImage ?? "") {
                    AsyncImage(url: imageURL) { phase in
                        switch phase {
                        case .empty:
                            ProgressView()
                        case .success(let image):
                            image.resizable()
                                .aspectRatio(contentMode: .fit)
                                .cornerRadius(10)
                        case .failure:
                            Image(systemName: "photo")
                                .resizable()
                                .scaledToFit()
                                .foregroundColor(.secondary)
                        @unknown default:
                            EmptyView()
                        }
                    }
                }
                
                Text(article.title ?? "Untitled")
                    .font(.title)
                    .fontWeight(.bold)
                    .padding(.top)
                
                if let author = article.author {
                    Text("By \(author)")
                        .font(.subheadline)
                        .foregroundColor(.secondary)
                }
                Text(article.publishedAt )
                    .font(.caption)
                    .foregroundColor(.gray)
                
                if let content = article.content {
                    Divider()
                    Text(content)
                        .font(.body)
                        .padding(.bottom)
                }
            }
            .padding()
        }
        .navigationTitle(Text(article.source.name ))
        .navigationBarTitleDisplayMode(.inline)
    }
}

struct SearchView: View {
    @State private var searchTerm: String = ""
    @ObservedObject var viewModel: NewsViewModel
    @Binding var isPresented: Bool
    
    var body: some View {
        NavigationView {
            VStack {
                TextField("Search", text: $searchTerm)
                    .textFieldStyle(RoundedBorderTextFieldStyle())
                    .padding()
                    .disableAutocorrection(true)
                    .onSubmit {
                        performSearch()
                    }
                
                Button("Search") {
                    performSearch()
                }
                .disabled(searchTerm.isEmpty)
                .padding()
                
                List {
                    if viewModel.isLoading {
                        ProgressView("Searching...")
                    } else if let errorMessage = viewModel.errorMessage {
                        Text(errorMessage)
                            .foregroundColor(.red)
                    } else {
                        ForEach(viewModel.articles, id: \.self) { article in
                            NavigationLink(destination: ArticleDetailView(article: article)) {
                                ArticleRow(article: article)
                            }
                        }
                    }
                }
            }
            .navigationBarTitle("Search News", displayMode: .inline)
            .navigationBarItems(trailing: Button(action: {
                isPresented = false
            }) {
                Image(systemName: "xmark")
            })
        }
    }
    
    private func performSearch() {
        if !searchTerm.isEmpty {
            viewModel.searchArticles(query: searchTerm)
        }
    }
    
}

struct MainView_Previews: PreviewProvider {
    static var previews: some View {
        MainView(viewModel: NewsViewModel(apiClient: MockNewsAPIClient()))
    }
}
 
