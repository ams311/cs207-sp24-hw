//
//  NewsApi.swift
//  hw5-ams311
//
//  Created by ams311 on 2/27/24.
//

import Foundation
protocol NewsAPI {
    func fetchTopHeadlines(country: String, completion: @escaping (Result<NewsResponse, APIError>) -> Void)
    func searchForArticles(with query: String, completion: @escaping (Result<NewsResponse, APIError>) -> Void)
}
