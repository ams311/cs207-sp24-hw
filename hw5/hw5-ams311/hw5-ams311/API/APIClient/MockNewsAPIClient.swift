import Foundation

class MockNewsAPIClient: NewsAPI {
    func fetchTopHeadlines(country: String, completion: @escaping (Result<NewsResponse, APIError>) -> Void) {
        // Simulated delay
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            // Example of a successful response
            let mockArticles = [Article(source: Source(id: nil, name: "Mock Source"),
                                         author: "Mock Author",
                                         title: "Mock Headline",
                                         description: "This is a mock description of a top headline.",
                                         url: "https://example.com/mockheadline",
                                         urlToImage: "https://example.com/mockimage.jpg",
                                         publishedAt: "2024-02-27T00:00:00Z",
                                         content: "This is mock content for a top headline.")]
            let mockNewsResponse = NewsResponse(status: "ok", totalResults: 1, articles: mockArticles)
            completion(.success(mockNewsResponse))
        }
    }

    func searchForArticles(with query: String, completion: @escaping (Result<NewsResponse, APIError>) -> Void) {
        // Simulated delay
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            // Example of a successful response for search
            if !query.isEmpty {
                let mockArticles = [Article(source: Source(id: nil, name: "Mock Source"),
                                             author: "Mock Author",
                                             title: "Search Result",
                                             description: "This is a mock description of a search result.",
                                             url: "https://example.com/mocksearchresult",
                                             urlToImage: "https://example.com/mocksearchimage.jpg",
                                             publishedAt: "2024-02-27T00:00:00Z",
                                             content: "This is mock content for a search result.")]
                let mockNewsResponse = NewsResponse(status: "ok", totalResults: 1, articles: mockArticles)
                completion(.success(mockNewsResponse))
            } else {
                // Simulating an error when query is empty
                completion(.failure(.requestError(400, "Query is empty")))
            }
        }
    }
}

