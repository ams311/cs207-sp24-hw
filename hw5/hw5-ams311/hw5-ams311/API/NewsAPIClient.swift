import Foundation

class NewsAPIClient: NewsAPI {
    private let apiKey = "bf5a2d4cfe03469082fef91285653282"
    private let session = URLSession.shared

    func fetchTopHeadlines(country: String, completion: @escaping (Result<NewsResponse, APIError>) -> Void) {
        guard let url = URL(string: "https://newsapi.org/v2/top-headlines?country=\(country)&apiKey=\(apiKey)") else {
            completion(.failure(.invalidUrl("https://newsapi.org/v2/top-headlines?country=\(country)&apiKey=\(apiKey)")))
            return
        }
        
        let task = session.dataTask(with: url) { data, response, error in
            if let error = error {
                completion(.failure(.networkingError(error)))
                return
            }
            
            guard let httpResponse = response as? HTTPURLResponse else {
                completion(.failure(.invalidResponse))
                return
            }
            
            switch httpResponse.statusCode {
            case 200...299:
                guard let data = data else {
                    completion(.failure(.resourceError("Data not found")))
                    return
                }
                
                do {
                    let decoder = JSONDecoder()
                    let newsResponse = try decoder.decode(NewsResponse.self, from: data)
                    completion(.success(newsResponse))
                } catch let decodingError as DecodingError {
                    completion(.failure(.decodingError(decodingError)))
                } catch {
                    completion(.failure(.failed))
                }
            case 400...499:
                completion(.failure(.requestError(httpResponse.statusCode, "Client Error")))
            case 500...599:
                completion(.failure(.serverError))
            default:
                completion(.failure(.invalidStatusCode("Status Code: \(httpResponse.statusCode)")))
            }
        }
        
        task.resume()
    }

    func searchForArticles(with query: String, completion: @escaping (Result<NewsResponse, APIError>) -> Void) {
        let encodedQuery = query.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""
        guard let url = URL(string: "https://newsapi.org/v2/everything?q=\(encodedQuery)&apiKey=\(apiKey)") else {
            completion(.failure(.invalidUrl("https://newsapi.org/v2/everything?q=\(encodedQuery)&apiKey=\(apiKey)")))
            return
        }
        
        let task = session.dataTask(with: url) { data, response, error in
            if let error = error {
                completion(.failure(.networkingError(error)))
                return
            }
            
            guard let httpResponse = response as? HTTPURLResponse else {
                completion(.failure(.invalidResponse))
                return
            }
            
            switch httpResponse.statusCode {
            case 200...299:
                guard let data = data else {
                    completion(.failure(.resourceError("Data not found")))
                    return
                }
                
                do {
                    let decoder = JSONDecoder()
                    let newsResponse = try decoder.decode(NewsResponse.self, from: data)
                    completion(.success(newsResponse))
                } catch let decodingError as DecodingError {
                    completion(.failure(.decodingError(decodingError)))
                } catch {
                    completion(.failure(.failed))
                }
            case 400...499:
                completion(.failure(.requestError(httpResponse.statusCode, "Client Error")))
            case 500...599:
                completion(.failure(.serverError))
            default:
                completion(.failure(.invalidStatusCode("Status Code: \(httpResponse.statusCode)")))
            }
        }
        
        task.resume()
    }
}

