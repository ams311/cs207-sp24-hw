//
//  ContentView.swift
//  hw0-ams311
//
//  Created by ams311 on 1/23/24.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        VStack {
            Image(systemName: "globe")
                .imageScale(.large)
                .foregroundStyle(.tint)
            Text("Hello, CS207!")
            Text("ams311 is here!")
        }
        .padding()
    }
}

#Preview {
    ContentView()
}
