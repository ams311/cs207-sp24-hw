//
//  hw0_ams311App.swift
//  hw0-ams311
//
//  Created by ams311 on 1/23/24.
//

import SwiftUI

@main
struct hw0_ams311App: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
