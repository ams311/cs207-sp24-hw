import SwiftUI

struct ContentView: View {
    @State private var numberevents = 0
    @State private var selectedAchievements: [Achievement] = []
    var isBingo: Bool {
        return selectedAchievements.count > 0 && selectedAchievements.allSatisfy { $0.achieved }
    }

    var body: some View {
        VStack {
            Text("Play Classroom Bingo")
                .font(.largeTitle)
            
            Stepper("Number Events", value: $numberevents, in: 0...5)
            
            Button(action: {
                selectedAchievements = Achievement.createCard(number: numberevents)
            }) {
                Text("Get New Card (\(numberevents) events)")
            }
            .padding(.top, 10)
            
            Spacer()
            
            VStack {
                ForEach(selectedAchievements.indices, id: \.self) { index in
                    HStack {
                        Button(action: {
                            selectedAchievements[index].achieved.toggle() // Toggle the achieved property
                        }) {
                            Text(selectedAchievements[index].prompt)
                
                        }
                        Spacer()
                        
                        if selectedAchievements[index].achieved {
                            Image(systemName: "checkmark")
                        } else{Image(systemName: "circle")}
                    }
                }
            }
            Spacer()
            Text(isBingo && numberevents > 0 ? "Bingo!" : "No bingo yet")
                .font(isBingo ? .largeTitle : .subheadline)
                .foregroundColor(isBingo ?  .red : .primary)
        }
        .padding()
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}


