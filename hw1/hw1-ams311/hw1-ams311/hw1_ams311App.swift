//
//  hw1_ams311App.swift
//  hw1-ams311
//
//  Created by ams311 on 1/26/24.
//

import SwiftUI

@main
struct hw1_ams311App: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
