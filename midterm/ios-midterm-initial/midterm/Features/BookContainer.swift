import SwiftUI
import SwiftData



struct BookContainer: View {
    @Environment(\.modelContext) var modelContext
    @Query var books: [Book]
    @State private var hideSpoilers: Bool = false
  
    var body: some View {
        VStack {
            BookList(books: books)
        }
        .onAppear {
            if books.isEmpty {
                for book in Book.previewData {
                    modelContext.insert(book)
                }
            }
        }
    }
}

#Preview {
  let preview = PreviewContainer([Book.self])
  preview.add(items: Book.previewData)
  return NavigationStack {
    BookContainer()
      .modelContainer (preview.container)
  }
}

