//
//  BookDetail.swift
//  midterm
//
//  Created by ams311 on 3/3/24.
//

import SwiftUI

struct BookDetail: View {
    @Bindable var book: Book
    @State private var synopsis: String? = nil
    @State private var isLoading = false
    @State private var showError = false
    @Environment(\.presentationMode) var presentationMode // For the return button
    
    var body: some View {
        ScrollView {
            VStack(alignment: .center, spacing: 20) {
                if let coverUrl = book.coverUrl {
                    AsyncImage(url: coverUrl) { image in
                        image.resizable()
                    } placeholder: {
                        ProgressView()
                    }
                    .scaledToFit()
                    .frame(width: 150, height: 200)
                }
                
                Text(book.title)
                    .font(.title)
                    .multilineTextAlignment(.center)
                
                Text("Author: \(book.author)")
                    .font(.subheadline)
                    .multilineTextAlignment(.center)
                
                Toggle(isOn: $book.readingList) {
                    Text(book.readingList ? "On Reading List" : "Add to Reading List")
                }
                
                if isLoading {
                    ProgressView()
                        .progressViewStyle(CircularProgressViewStyle())
                } else if let synopsis = synopsis {
                    Text(synopsis)
                } else if showError {
                    Text("Could not load synopsis.")
                }
            }
            .padding()
        }
        .onAppear(perform: loadSynopsis)
        .navigationTitle("Book Details")
        .navigationBarTitleDisplayMode(.inline)
        .navigationBarItems(
            trailing: Button(action: {
                book.readingList.toggle()
            }) {
                Text(book.readingList ? "Remove from List" : "Add to List")
            })
    }
    
    private func loadSynopsis() {
        isLoading = true
        OpenLibraryAPIService().fetchSynopsis(forBook: book.id) { result in
            DispatchQueue.main.async {
                isLoading = false
                switch result {
                case .success(let fetchedSynopsis):
                    synopsis = fetchedSynopsis
                case .failure:
                    showError = true
                }
            }
        }
    }
}
#Preview {
  let preview = PreviewContainer([Book.self])
  let Book = Book.previewData[0]
  return NavigationStack {
    BookDetail(book: Book)
      .modelContainer (preview.container)
  }
}

