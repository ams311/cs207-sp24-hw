//
//  BookForm.swift
//  midterm
//
//  Created by ams311 on 3/3/24.
//

import SwiftUI

struct BookForm: View {
    @Binding var data: Book.FormData
    
    
    var body: some View {
        Form {
            TextFieldWithLabel(label: "ID", text: $data.id, prompt: "Enter a Book ID")
            TextFieldWithLabel(label: "Title", text: $data.title, prompt: "Enter a Book title")
            TextFieldWithLabel(label: "Author", text: $data.author, prompt: "Enter a Book Author")
            
        }
    }
}
#Preview {
  let preview = PreviewContainer([Book.self])
  let data = Binding.constant(Book.previewData[0].dataForForm)
  return BookForm(data: data)
      .modelContainer(preview.container)
}
