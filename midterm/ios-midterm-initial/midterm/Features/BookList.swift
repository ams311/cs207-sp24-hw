import SwiftUI
import SwiftData
struct BookList: View {
    @Environment(\.modelContext) private var modelContext
    var books: [Book]
    @State private var isPresentingBookForm: Bool = false
    @State private var newBookFormData = Book.FormData()
    
    var body: some View {
        NavigationView {
            List(books.sorted(by: { $0.title < $1.title }), id: \.id) { book in
                NavigationLink(destination: BookDetail(book: book)) {
                    HStack {
                        if let coverUrl = book.coverUrl {
                            AsyncImage(url: coverUrl) { image in
                                image.resizable().aspectRatio(contentMode: .fit)
                            } placeholder: {
                                ProgressView()
                            }
                            .frame(width: 50, height: 50)
                        }
                        VStack(alignment: .leading) {
                            Text(book.title).font(.headline)
                            Text(book.author)
                        }
                    }
                }
            }
            .navigationTitle("Books")

            .toolbar {
                ToolbarItem(placement: .navigationBarTrailing) {
                    Button("Add") {
                        isPresentingBookForm.toggle()
                    }
                }
            }
            .sheet(isPresented: $isPresentingBookForm) {
                NavigationStack {
                    BookForm(data: $newBookFormData)
                        .toolbar {
                            ToolbarItem(placement: .navigationBarLeading) {
                                Button("Cancel") { isPresentingBookForm.toggle()
                                    newBookFormData = Book.FormData()
                                }
                            }
                            ToolbarItem(placement: .navigationBarTrailing) {
                                Button("Save") {
                                    Book.create(from: newBookFormData, context: modelContext)
                                    newBookFormData = Book.FormData()
                                    isPresentingBookForm.toggle()                }
                            }
                        }
                        .navigationTitle("Add Book")

                }
        

            }
        }
    }
}
// Assuming PreviewContainer setup is similar for Books
#Preview {
  let preview = PreviewContainer([Book.self])
  preview.add(items: Book.previewData)
  return NavigationView {
  BookList(books: Book.previewData)
      .modelContainer (preview.container)
  }}
