import SwiftUI
import SwiftData

import SwiftUI

struct ReadingList: View {
    @Environment(\.modelContext) private var modelContext
    @Query(filter: #Predicate<Book> { $0.readingList},
           sort: \Book.title)
    var readingListBooks: [Book]

    var body: some View {
        NavigationView {
            List {
                ForEach(readingListBooks.sorted(by: { $0.title < $1.title }), id: \.id) { book in
                    NavigationLink(destination: BookDetail(book:book)) {
                        HStack {
                            if let coverUrl = book.coverUrl {
                                AsyncImage(url: coverUrl) { image in
                                    image.resizable().aspectRatio(contentMode: .fit)
                                } placeholder: {
                                    ProgressView()
                                }
                                .frame(width: 50, height: 50)
                            }
                            VStack(alignment: .leading) {
                                Text(book.title).font(.headline)
                                Text(book.author).font(.subheadline)
                            }
                        }
                    }
                }
                .onDelete(perform: removeFromReadingList)
            }
            .navigationTitle("Reading List")
            .toolbar {
                EditButton()
            }
        }

    }

    private func removeFromReadingList(at offsets: IndexSet) {
           for index in offsets {
               let book = readingListBooks[index]
               // Update the book's readingList status instead of deleting it
               book.readingList = false
               
               // Save the context to persist changes
               do {
                   try modelContext.save()
               } catch {
                   // Handle the Core Data error
                   print("Unable to save changes to remove book from reading list: \(error.localizedDescription)")
               }
           }
       }
}

#Preview {
  let preview = PreviewContainer([Book.self])
  preview.add(items: Book.previewData)
  return
    ReadingList()
      .modelContainer (preview.container)
}
