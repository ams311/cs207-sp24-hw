//
//  OpenLibraryEndpoint.swift
//  midterm
//
//  Created by ams311 on 3/3/24.
//

import Foundation

struct OpenLibraryEndpoint {
    let bookID: String

    var url: URL? {
        var components = URLComponents()
        components.scheme = "https"
        components.host = "openlibrary.org"
        components.path = "/works/\(bookID).json"
        return components.url
    }
}
