// OpenLibraryAPIService.swift

// Modified OpenLibraryAPIService.swift with error handling

import Foundation

struct OpenLibraryAPIService {
    func fetchSynopsis(forBook bookID: String, completion: @escaping (Result<String, APIError>) -> Void) {
        guard let url = OpenLibraryEndpoint(bookID: bookID).url else {
            completion(.failure(APIError.invalidUrl("https://openlibrary.org/works/\(bookID).json")))
            return
        }

        let task = URLSession.shared.dataTask(with: url) { data, response, error in
            if let error = error {
                completion(.failure(APIError.networkingError(error)))
                return
            }

            guard let httpResponse = response as? HTTPURLResponse else {
                completion(.failure(APIError.invalidResponse))
                return
            }

            guard (200...299).contains(httpResponse.statusCode) else {
                if (500...599).contains(httpResponse.statusCode) {
                    completion(.failure(APIError.serverError))
                } else {
                    completion(.failure(APIError.requestError(httpResponse.statusCode, "Request failed with status code")))
                }
                return
            }

            guard let data = data else {
                completion(.failure(APIError.resourceError("No data received")))
                return
            }

            do {
                let json = try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
                if let description = json?["description"] as? [String: Any], let synopsis = description["value"] as? String {
                    completion(.success(synopsis))
                } else {
                    completion(.failure(APIError.resourceError("Synopsis not found")))
                }
            } catch {
                completion(.failure(APIError.decodingError(error as! DecodingError)))
            }
        }
        task.resume()
    }
}
